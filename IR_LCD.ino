#include <LiquidCrystal.h>
#include <IRremote.h>

#define LED_BUILTIN 13

#define RECEIVER 1
#define SENDER 2
#define ERROR 3

const byte MaxByteArraySize = 50;

int RECV_PIN = 2;
int LED_PIN = 3;

IRrecv irrecv(RECV_PIN,13);
IRsend irsend;

byte sendbuf[MaxByteArraySize] = {0};

decode_results results;
LiquidCrystal lcd(8,9,4,5,6,7);
char inputString[MaxByteArraySize];   
boolean stringComplete = false;

void setup()
{
//	inputString.reserve(40);
	Serial.begin(115200);
	pinMode(LED_BUILTIN, OUTPUT);
	irrecv.enableIRIn(); // Start the receiver
	lcd.begin(16, 2);
	lcd.print("Ready!");
}

// read serial
void serialEvent() {
  static char bufptr=0;
  volatile char inChar;
  int i;
  while (Serial.available()) {
    inChar = (char)Serial.read();
    inputString[bufptr++] = inChar;
    delay(10);
  }
  if (inChar == '\n' || inChar == '\r' || inChar == '!') {
    stringComplete = true;
    inputString[bufptr]=0;
    bufptr=0;
  }
}

unsigned long char_to_ul (byte *byteArray, byte bsize=4) {
  unsigned long r=0l;
  for (byte i=0;i<bsize;i++) {
    r=r<<8;
    r=r+byteArray[i];
  }
  return r;
}

void hexCharacterStringToBytes(byte *byteArray, const char *hexString, byte startindex=0)
{
  bool oddLength = (strlen(hexString) - startindex) & 1;

  byte currentByte = 0;
  byte byteIndex = 0;

  for (byte charIndex = startindex; charIndex < strlen(hexString); charIndex++)
  {
    bool oddCharIndex = charIndex & 1;

    if (oddLength)
    {
      // If the length is odd
      if (oddCharIndex)
      {
        // odd characters go in high nibble
        currentByte = nibble(hexString[charIndex]) << 4;
      }
      else
      {
        // Even characters go into low nibble
        currentByte |= nibble(hexString[charIndex]);
        byteArray[byteIndex++] = currentByte;
        currentByte = 0;
      }
    }
    else
    {
      // If the length is even
      if (!oddCharIndex)
      {
        // Odd characters go into the high nibble
        currentByte = nibble(hexString[charIndex]) << 4;
      }
      else
      {
        // Odd characters go into low nibble
        currentByte |= nibble(hexString[charIndex]);
        byteArray[byteIndex++] = currentByte;
        currentByte = 0;
      }
    }
  }
}

void dumpByteArray(const byte * byteArray, const byte arraySize)
{

  for (int i = 0; i < arraySize; i++)
  {
    Serial.print("0x");
    if (byteArray[i] < 0x10)
      Serial.print("0");
    Serial.print(byteArray[i], HEX);
    Serial.print(", ");
  }
  Serial.println();
}

byte nibble(char c)
{
  if (c >= '0' && c <= '9')
    return c - '0';

  if (c >= 'a' && c <= 'f')
    return c - 'a' + 10;

  if (c >= 'A' && c <= 'F')
    return c - 'A' + 10;

  return 0;  // Not a valid hexadecimal character
}

/*
void strtoarray () {
	for (int i=1; i < inputString.length(); i++ ){
		Serial.print("strtoarray: i=");
		Serial.println(i);
	    sendbuf[i]=chartoi(inputString[i]);
    Serial.print("strtoarray: sendbuf[i]=");
    Serial.println(sendbuf[i]);
	}
}
*/
void dumpsendbuf () {
	lcd.setCursor(0,1);
	for (int i =0; i < sizeof(sendbuf); i++ ){
		lcd.print(sendbuf[i],HEX);
	}
}

void dump(decode_results *results) {
  // Dumps out the decode_results structure.
  // Call this after IRrecv::decode()
  int count = results->rawlen;
  lcd.clear();
//  lcd.autoscroll();
  if (results->decode_type == UNKNOWN) {
    lcd.print("Unknown encoding: ");
    Serial.print("Unknown encoding: ");
  }
  else if (results->decode_type == NEC) {
    lcd.print("NEC: ");
    Serial.print("NEC: ");

  }
  else if (results->decode_type == SONY) {
    lcd.print("SONY: ");
    Serial.print("SONY: ");
  }
  else if (results->decode_type == RC5) {
    lcd.print("RC5: ");
    Serial.print("RC5: ");
  }
  else if (results->decode_type == RC6) {
    lcd.print("RC6: ");
    Serial.print("RC6: ");
  }
  else if (results->decode_type == PANASONIC) {
    lcd.print("PANASNC-Addr: ");
    Serial.print("PANASNC-Addr: ");
    lcd.print(results->address, HEX);
    Serial.print(results->address, HEX);
    lcd.print(" Value: ");
    Serial.print(" Value: ");
  }
  else if (results->decode_type == LG) {
    lcd.print("LG: ");
    Serial.print("LG: ");
  }
  else if (results->decode_type == JVC) {
    lcd.print("JVC: ");
    Serial.print("JVC: ");
  }
  else if (results->decode_type == AIWA_RC_T501) {
    lcd.print("AIWA RC T501: ");
    Serial.print("AIWA RC T501: ");
  }
  else if (results->decode_type == WHYNTER) {
    lcd.print("Whynter: ");
    Serial.print("Whynter: ");
  }

  Serial.print(results->bits, DEC);
  Serial.println(" bits");

  lcd.print(results->bits, DEC);
  lcd.print(" bits");
  lcd.setCursor(0, 1);
  lcd.print(results->value, HEX);

  Serial.println(results->value, HEX);

  Serial.print("Decoded Raw (");
  Serial.print(count, DEC);
  Serial.print("): ");
  for (int i = 1; i < count; i++) {
    if (i & 1) {
      Serial.print(results->rawbuf[i]*USECPERTICK, DEC);
    }
    else {
      Serial.print('-');
      Serial.print((unsigned long) results->rawbuf[i]*USECPERTICK, DEC);
    }
    Serial.print(" ");
  }
  Serial.println("");

}


void loop()
{
  byte sendbuf_len;
  bool commend_executed=false;
	if (irrecv.decode(&results)) {
		digitalWrite(LED_BUILTIN, HIGH);
//	    lcd.clear();
//	    lcd.print(results.value, HEX);
	    dump(&results);
//	    lcd.setCursor(0, 0);
	    irrecv.resume(); // Receive the next value
	    delay(200);                       // wait for a second
  		digitalWrite(LED_BUILTIN, LOW);
	}
	if (stringComplete) {
//    Serial.println("Received command");
//    dumpByteArray(inputString,sizeof(inputString));
   // wait a bit for the entire message to arrive
//    	inputString.trim();
	    // clear the screen
	   lcd.clear();
//    	lcd.print(inputString);
//    	switch (inputString.charAt(0)){
     hexCharacterStringToBytes(sendbuf, inputString, 1);
     sendbuf_len=(strlen(inputString) -1)/2;
      switch (inputString[0]){
    		case 'N':	// NEC
      		lcd.print("Send NEC:");
//          Serial.print("Send NEC: 0x");
          unsigned long d = char_to_ul(sendbuf,4);
          lcd.setCursor(0,1);
          lcd.print("0x");
          lcd.print(d,HEX);
//          Serial.println(d,HEX);
          irsend.sendNEC(char_to_ul(sendbuf),32);
          commend_executed=true;
    		break;
    		case 'S':	// Sony
    		break;
    		case '5':	// RC5
    		break;
    		case '6':	// RC6
    		break;
    		case 'P':	// Panasonic
    		break;
    		case 'L':	// LG
    		break;
    		case 'J':	// JVC
    		break;
    		case 'A':	// Aiwa
    		break;
    		case 'W':	// Whynter
    		break;
    		case 'R':	// RAW
    		break;
        default:
          byte bufptr=0;
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print(inputString);
/*          Serial.print("inputString=");
          Serial.print(inputString);
          while (inputString[bufptr]!=0) {
            lcd.print(inputString[bufptr]);
            Serial.print(inputString[bufptr]);
            bufptr++;
          }
          Serial.println();
*/
        break;
    	}
//	    inputString = "";
//      if (commend_executed) {
        inputString[0] = 0;
        stringComplete = false;
        lcd.setCursor(0,0);
        irrecv.enableIRIn();
//      }
	}

}

